set tabstop=8
set softtabstop=4
set shiftwidth=4
set expandtab
set encoding=utf-8
set termencoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8
set hls

syntax on
set autoindent
nmap wm :set number<cr>:only<cr>:Vexplore 20<cr>
